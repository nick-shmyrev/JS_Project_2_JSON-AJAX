"use strict";



var btnCalendar = document.querySelector("#btnCalendar");
var calendar = document.querySelector(".calendar");
btnCalendar.addEventListener("click", function(){
  if (calendar.style.display !== null){ calendar.style.display = ""; }
  else{ calendar.style.display = "none"; }

});

/*
██████   █████  ████████ ███████ ██████  ██  ██████ ██   ██ ███████ ██████
██   ██ ██   ██    ██    ██      ██   ██ ██ ██      ██  ██  ██      ██   ██
██   ██ ███████    ██    █████   ██████  ██ ██      █████   █████   ██████
██   ██ ██   ██    ██    ██      ██      ██ ██      ██  ██  ██      ██   ██
██████  ██   ██    ██    ███████ ██      ██  ██████ ██   ██ ███████ ██   ██
*/

// Fill in the year and month lists
fillYears("#ddlYear", 2015, 2017);
fillMonths("#ddlMonth", 0, 11);

// Add event listeners to lists, fill in the days for specific month & year
var ddlYear = document.querySelector("#ddlYear");
var ddlMonth = document.querySelector("#ddlMonth");
ddlYear.addEventListener("change", updateDays);
ddlMonth.addEventListener("change", updateDays);
function updateDays(){
  if((ddlYear.value !== "-1") && (ddlMonth.value !== "-1")){
    fillDays("table", ddlYear.value, ddlMonth.value);
  }
}

function fillYears(yearsList, minYear, maxYear){
  var yearsList = document.querySelector(yearsList);
  var counter = 1;
  yearsList.options.length = 0;
  yearsList.options[0] = new Option("--Year--", -1);
  for(var year = minYear; year <= maxYear; year++){
    yearsList.options[counter++] = new Option(year, year);
  }
}

function fillMonths(monthList, minMonth, maxMonth){
  var monthList = document.querySelector(monthList);
  var monthArray = ["January", "February", "March", "April",
                    "May", "June", "July", "August",
                    "September", "October", "November", "December"]
  var counter = 1;
  monthList.options.length = 0;
  monthList.options[0] = new Option("--Month--", -1);
  for(var i = minMonth; i <= maxMonth; i++){
    monthList.options[counter++] = new Option(monthArray[i], i);
  }
}

function fillDays(tableID, year, month){
  var startDay = new Date(year, month, 1).getDay();
  // starts the week from monday instead of sunday
  startDay = (startDay===0) ? 7 : startDay;
  var endDay = monthLength(month);
  var tbody = document.querySelector(tableID + " tbody");
  var numRows = tbody.rows.length;
  // clears all tbody rows before filling with days
  for(var i = 1; i <= numRows; i++){ tbody.deleteRow(-1); }

  // Insert first table row
  var row = tbody.insertRow(-1);
  // Create index var, used to measure max <tr> length
  var cellIndex = 0;
  // Insert empty <td> representing previous month days
  for (var i = 1; i < startDay; i++){
    row.insertCell(cellIndex++).innerHTML = "";
  }
  // Insert current month days
  for(var i = 1; i <= endDay; i++){
    // When inserted 7 cells, start a new row, reset cell index
    if (cellIndex === 7) {
      row = tbody.insertRow(-1);
      cellIndex = 0;
    }
    row.insertCell(cellIndex++).innerHTML = "<button type='button' class='btnDay' onclick='clickBtnDay(this)' value='" + i + "'>" + i + "</button>";
  }

  // Determines the max lenght of month
  function monthLength(month){
    switch (month) {
      case "1":
        return (isLeapYear(year)) ? 29 : 28;
      case "3": case "5": case "8": case "10":
        return 30;
      default:
        return 31;
    }
  } // END monthLength

  // Determines if year isLeapYear
  function isLeapYear(year){
    // 1. If the year is evenly divisible by 4, go to step 2. Otherwise, go to step 5.
    if(year % 4 === 0){
      // 2. If the year is evenly divisible by 100, go to step 3. Otherwise, go to step 4.
      if(year % 100 === 0){
        // 3. If the year is evenly divisible by 400, go to step 4. Otherwise, go to step 5.
        if(year % 400 === 0){
          return true;
        }
        else{ return false; }
      }
      else{ return true; }
    }
    else{ return false; }
    // 4. The year is a leap year (it has 366 days).
    // 5. The year is not a leap year (it has 365 days).
  } // END isLeapYear

} // END fillDAys









/*
     ██ ███████  ██████  ███    ██     ██  █████       ██  █████  ██   ██
     ██ ██      ██    ██ ████   ██    ██  ██   ██      ██ ██   ██  ██ ██
     ██ ███████ ██    ██ ██ ██  ██   ██   ███████      ██ ███████   ███
██   ██      ██ ██    ██ ██  ██ ██  ██    ██   ██ ██   ██ ██   ██  ██ ██
 █████  ███████  ██████  ██   ████ ██     ██   ██  █████  ██   ██ ██   ██
*/



function clickBtnDay(btn){
  if((ddlYear.value !== "0") && (ddlMonth.value !== "0")){
    var year = ddlYear.value;
    var month = parseInt(ddlMonth.value) + 1; // Because month values are 0-based, yet jsonUrl uses non-0-based values
    var day = btn.value;
    var lblCurrentDate = document.querySelector("#lblCurrentDate");
    var jsonUrl = "http://gd2.mlb.com/components/game/mlb/year_" + lPad(year, 2, 0) + "/month_" + lPad(month, 2, 0) + "/day_" + lPad(day, 2, 0) + "/master_scoreboard.json";
    lblCurrentDate.innerHTML = year + "–" + lPad(month, 2, 0) + "–" + lPad(day, 2, 0);
    calendar.style.display = "none";
    flushData();
    extractProperties( jsonUrl );
  }
}


function lPad(string, length, pad_string){
  return (string.toString().length < length) ? lPad( (pad_string.toString() + string), length, pad_string) : string;
}




var dataSubset = {};

function extractProperties(jsonUrl){

  // Assings to variable the result of syncCall, that is parsed to JSON
  var jsonData = JSON.parse( syncCall(jsonUrl) );

  function syncCall(url){
    var response = "";
    var request = new XMLHttpRequest();

    if (request !== null){
      // 3rd key = async parameter. false - sync, true - async
      request.open("GET", url, false);
      request.onload = function(){ response = request.responseText; };
      request.onerror = function(){ console.log('Connection error'); };
      request.send();
    }
    return response;
  }

  dataSubset.copyright = jsonData.copyright;
  dataSubset.data = {};
  dataSubset.data.games = {};
  dataSubset.data.games.game = [];
  // if jsonData has game, and game is array, copy the properties.
  if (jsonData.data.games.hasOwnProperty("game") && (Array.isArray(jsonData.data.games.game))){
    var objOrig = jsonData.data.games;
    var objCopy = dataSubset.data.games;
    for (var i = 0; i < objOrig.game.length; i++){
      objCopy.game[i] = {};
      objCopy.game[i].home_team_name = (objOrig.game[i].hasOwnProperty("home_team_name")) ? objOrig.game[i].home_team_name : "";
      objCopy.game[i].away_team_name = (objOrig.game[i].hasOwnProperty("away_team_name")) ? objOrig.game[i].away_team_name : "";
      objCopy.game[i].winning_pitcher = {};
      objCopy.game[i].winning_pitcher.first = (objOrig.game[i].hasOwnProperty("winning_pitcher")) ? objOrig.game[i].winning_pitcher.first : ""; // Because you can trust no one and have to validate all incoming data, see examples date 2015-07-06 game[6], 2016–04–01 game[0] game[11]
      objCopy.game[i].winning_pitcher.last = (objOrig.game[i].hasOwnProperty("winning_pitcher")) ? objOrig.game[i].winning_pitcher.last : "";
      objCopy.game[i].losing_pitcher = {};
      objCopy.game[i].losing_pitcher.first = (objOrig.game[i].hasOwnProperty("losing_pitcher")) ? objOrig.game[i].losing_pitcher.first : "";
      objCopy.game[i].losing_pitcher.last = (objOrig.game[i].hasOwnProperty("losing_pitcher")) ? objOrig.game[i].losing_pitcher.last : "";
      objCopy.game[i].venue = (objOrig.game[i].hasOwnProperty("venue")) ? objOrig.game[i].venue : "";
    }
    // upon copying all the needed properties, display the first game
    printObjects(dataSubset, 0);
  }
  // ELSE IF jsonData has game, but it's not an array, still copy the properties.
  else if (jsonData.data.games.hasOwnProperty("game") && (!Array.isArray(jsonData.data.games.game))) { // Again, trust no one and validate all data, see example date 2015-03-01
    var objOrig = jsonData.data.games;
    var objCopy = dataSubset.data.games;
    objCopy.game[0] = {};
    objCopy.game[0].home_team_name = (objOrig.game.hasOwnProperty("home_team_name")) ? objOrig.game.home_team_name : "";
    objCopy.game[0].away_team_name = (objOrig.game.hasOwnProperty("away_team_name")) ? objOrig.game.away_team_name : "";
    objCopy.game[0].winning_pitcher = {};
    objCopy.game[0].winning_pitcher.first = (objOrig.game.hasOwnProperty("winning_pitcher")) ? objOrig.game.winning_pitcher.first : "";
    objCopy.game[0].winning_pitcher.last = (objOrig.game.hasOwnProperty("winning_pitcher")) ? objOrig.game.winning_pitcher.last : "";
    objCopy.game[0].losing_pitcher = {};
    objCopy.game[0].losing_pitcher.first = (objOrig.game.hasOwnProperty("losing_pitcher")) ? objOrig.game.losing_pitcher.first : "";
    objCopy.game[0].losing_pitcher.last = (objOrig.game.hasOwnProperty("losing_pitcher")) ? objOrig.game.losing_pitcher.last : "";
    objCopy.game[0].venue = (objOrig.game.hasOwnProperty("venue")) ? objOrig.game.venue : "";

    // upon copying all the needed properties, display the first game
    printObjects(dataSubset, 0);
  }
  // Else, if there's no game, display error.
  else { alert("No games on this date, pick another one."); }
}

// prints properties of particular game to textboxes
function printObjects(object, index){
  var obj = object.data.games;
  document.querySelector("#txtHomeTeamName").value = obj.game[index].home_team_name;
  document.querySelector("#txtAwayTeamName").value = obj.game[index].away_team_name;
  document.querySelector("#txtWinningPitcher").value = (obj.game[index].winning_pitcher.last === "" || obj.game[index].winning_pitcher.first === "") ? "" : obj.game[index].winning_pitcher.last + ", " + obj.game[index].winning_pitcher.first;
  document.querySelector("#txtLosingPitcher").value = (obj.game[index].losing_pitcher.last === "" || obj.game[index].losing_pitcher.first === "") ? "" : obj.game[index].losing_pitcher.last + ", " + obj.game[index].losing_pitcher.first;
  document.querySelector("#txtVenue").value = obj.game[index].venue;
}



var txtHomeTeamName = document.querySelector("#txtHomeTeamName");
var txtAwayTeamName = document.querySelector("#txtAwayTeamName");
var txtWinningPitcher = document.querySelector("#txtWinningPitcher");
var txtLosingPitcher = document.querySelector("#txtLosingPitcher");
var txtVenue = document.querySelector("#txtVenue");
var alphaRegEx = /^[a-z\s&-]*$|^$/i;
var nameRegEx = /^[a-z'\s-]{1,},\s[a-z'\s-]{1,}$|^$/i;
var venueRegEx = /^[a-z',\s&-]*$|^$/i;

txtHomeTeamName.addEventListener("input", function(){
  if (!alphaRegEx.test(this.value)) { this.style.border = "1px solid red"; }
  else { this.style.border = ""; }
});
txtAwayTeamName.addEventListener("input", function(){
  if (!alphaRegEx.test(this.value)) { this.style.border = "1px solid red"; }
  else { this.style.border = ""; }
});
txtWinningPitcher.addEventListener("input", function(){
  if (!nameRegEx.test(this.value)) { this.style.border = "1px solid red"; }
  else { this.style.border = ""; }
});
txtLosingPitcher.addEventListener("input", function(){
  if (!nameRegEx.test(this.value)) { this.style.border = "1px solid red"; }
  else { this.style.border = ""; }
});
txtVenue.addEventListener("input", function(){
  if (!venueRegEx.test(this.value)) { this.style.border = "1px solid red"; }
  else { this.style.border = ""; }
});

// Clears all the texboxes. resets index var back to zero
function flushData(){
  // reset index back to zero each time new set of data is loaded
  gameIndex = 0;
  txtHomeTeamName.value = "";
  txtAwayTeamName.value = "";
  txtWinningPitcher.value = "";
  txtLosingPitcher.value = "";
  txtVenue.value = "";
}

var gameIndex = 0;
var btnPrevious = document.querySelector("#btn-game-prev");
var btnSave = document.querySelector("#btn-game-save");
var btnNext = document.querySelector("#btn-game-next");

btnPrevious.addEventListener("click", function(){
  if (gameIndex > 0) {printObjects(dataSubset, --gameIndex);}
});
btnSave.addEventListener("click", function(){
  // IF dataSubset has been populated with data and all textboxes are valid, save modified values back to dataSubset
  if(dataSubset.hasOwnProperty("data") &&
    alphaRegEx.test(txtHomeTeamName.value) &&
    alphaRegEx.test(txtAwayTeamName.value) &&
    nameRegEx.test(txtWinningPitcher.value) &&
    nameRegEx.test(txtLosingPitcher.value) &&
    venueRegEx.test(txtVenue.value))
    {
    dataSubset.data.games.game[gameIndex].home_team_name = txtHomeTeamName.value;
    dataSubset.data.games.game[gameIndex].away_team_name = txtAwayTeamName.value;
    var winning_pitcher = txtWinningPitcher.value.split(", ");
    dataSubset.data.games.game[gameIndex].winning_pitcher.first = winning_pitcher[1];
    dataSubset.data.games.game[gameIndex].winning_pitcher.last = winning_pitcher[0];
    var losing_pitcher = txtLosingPitcher.value.split(", ");
    dataSubset.data.games.game[gameIndex].losing_pitcher.first = losing_pitcher[1];
    dataSubset.data.games.game[gameIndex].losing_pitcher.last = losing_pitcher[0];
    dataSubset.data.games.game[gameIndex].venue = txtVenue.value;
  }
});
btnNext.addEventListener("click", function(){
  if (gameIndex < dataSubset.data.games.game.length - 1) {printObjects(dataSubset, ++gameIndex);}
});
